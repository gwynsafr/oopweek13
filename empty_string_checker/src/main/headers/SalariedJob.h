#ifndef UNTITLEDCLASSES_SALARIEDJOB_H
#define UNTITLEDCLASSES_SALARIEDJOB_H

#include <iostream>
#include <string>
#include <utility>

#include "Job.h"

class SalariedJob : private Job {

public:
  double jobSalary = 0.0;

  SalariedJob(long id, std::string name, double jobSalary);
  SalariedJob();

  friend std::ostream &operator<<(std::ostream &os,
                                  const SalariedJob &salariedjob) {
    std::cout << salariedjob.getName()<< " | " << salariedjob.jobSalary
              << std::endl;
  };

  const double &getjobSalary() const { return this->jobSalary; };
  void setjobSalary(const double &jobnewsalary) {
    this->jobSalary = jobnewsalary;
  };

  double calculatePayment();
};

#endif // UNTITLEDCLASSES_SALARIEDJOB_H
#ifndef UNTITLEDCLASSES_ContractReport_H
#define UNTITLEDCLASSES_ContractReport_H

#include <iostream>
#include <string>
#include <utility>

#include "Datacollection.h"

class ContractReport {
public:
  void print(std::list<Contract>);
};

#endif // UNTITLEDCLASSES_ContractReport_H
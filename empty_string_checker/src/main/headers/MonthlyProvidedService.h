#ifndef UNTITLEDCLASSES_MONTHLYSERVICE_H
#define UNTITLEDCLASSES_MONTHLYSERVICE_H

#include <iostream>
#include <string>
#include <utility>

#include "ProvidedService.h"

class Monthlyservice : private ProvidedService {
private:
  double ServiceCostPerMonth = 0.0;
public:
  Monthlyservice(int id, std::string name, double newServiceCostPerMonth,
                 std::string description);

  Monthlyservice();

  friend std::ostream &operator<<(std::ostream &os,
                                  const Monthlyservice &service) {
    std::cout << service.getName() << " | " << service.ServiceCostPerMonth << " | "
              << service.getDescription() << std::endl;
  };

  const double &getServiceCostPerMonth() const {
    return this->ServiceCostPerMonth;
  };
  void setServiceCostPerMonth(const double &newServiceCostPerMonth) {
    this->ServiceCostPerMonth = newServiceCostPerMonth;
  };
  void recommendService();
};

#endif // UNTITLEDCLASSES_MONTHLYSERVICE_H
#include "ContractService.h"

std::list<Contract> ContractService::getActiveContracts(std::list<Contract> contracts) {
  std::list<Contract> ActiveContracts;
  for (Contract contract : contracts) {
    if (contract.getStatus() == "Active") {
      ActiveContracts.push_back(contract);
    }
  }
  return ActiveContracts;
}
